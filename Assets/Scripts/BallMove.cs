using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{
    public float speed = 30;

    // Start is called before the first frame update
    void Start()
    {
        //(1, 0)
        //(-1. 0)
        float x;
        if (Random.Range(0, 2) == 0)
        {
            x = 1;
        }
        else
        {
            x = -1;
        }
        Vector2 direction = new Vector2(x, 0);
        GetComponent<Rigidbody2D>().velocity = direction * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
